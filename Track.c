/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - track.c
 * Last updated - 18/10/2018
 * Function: ADT for storing a Duration with a title
 *          - Initialiser
 *          - Getters for title and duration
 *          - Free struct
 *          - test harness
 */

#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include "track.h"
#include "duration.h"
#include "utils.h"

//----------------------------INITIALISER----------------------

Track* track_New(char *ti, Duration *dur)
{
    Track *t = (Track*)malloc(sizeof(Track));
    
    if( t == NULL)
    {
        fprintf(stdout,"Not enough memory could be allocated!");
    }

    strncpy(t->title,ti,100);

    t->duration = dur;
    return t;
}
//---------------FUNCTIONS---------------------------------------
Duration* track_GetDuration(Track* t) //get duration
{
    return t->duration;
}

char* track_GetTitle(Track* t) //get title
{
    return t->title;
}

void track_Free(Track* t) //free memory
{
    duration_Free(track_GetDuration(t));
    free(t->title);
    free(t);
}

void track_Printf(FILE *fd,Track *t) //formatted print to stream
{
    fprintf(fd,"%s : ",track_GetTitle(t));
    duration_Printf(fd,track_GetDuration(t));
}
//Compares 2 track ADT's, returns 1 if arg1 is greater than, 0 if equal, and
//-1 if less than arg2 alphabetically 
int track_CompareTitle(Track* t1, Track* t2)
{
    int compare1 = strcmp(track_GetTitle(t1),track_GetTitle(t2));
    int compare2 = strcmp(track_GetTitle(t2),track_GetTitle(t1));
    //more than (inverted as strcmp returns difference which is negative if >)
    if(compare1 < compare2) 
    {
        return 1;
    }
    else if(compare1 == compare2) //equal
    {
        return 0;
    }
    else            //therefore less than
    {
        return -1;
    }
    
}
int track_CompareDuration(Track* t1, Track* t2) //same return as comapare title 
{   //uses compare function built into duration
    return duration_Compare(track_GetDuration(t1),track_GetDuration(t2));
}

