/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - album.h
 * Last updated - 18/10/2018
 * Function: Header file for the Album ADT
 *          - Provides declaration for struct and typedef
 *          - 
 */

#ifndef ALBUM_H  
#define ALBUM_H

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct AlbumStruct
    {
        char *title;
        char *artist;
        Track **tracks;
        int size;
        int index;
    }Album;
    
    //new Album ADT
    Album *album_New(char ti[], char ar[], int s); 
    void *album_Printf(FILE *fd,Album *a); //print album
    char *album_GetArtist(Album *a); //return album artist
    char *album_GetTitle(Album *a); //return album title
    Track **album_GetTracks(Album *a); //return track array
    int album_GetSize(Album *a); //return max track array size
    void *album_Free(Album *a); //free album
    void *album_AddTrack(Album *a,Track *t); //add track to collection
    int album_Compare(const void *a1, const void *a2); //compare album artists
    int album_CompareSongNum(Album *a,Album *a2); //compare songs in albums
    Track *album_GetTrack(Album *a, int index);//get track by index
    int album_GetIndex(Album *a);//get current number of songs
    
#ifdef __cplusplus
}
#endif

#endif /* ALBUM_H */

