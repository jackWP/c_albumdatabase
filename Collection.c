/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - collection.h
 * Last updated - 18/10/2018
 * Function: 
 *          - 
 */
#include <stdio.h>
#include <stdlib.h>
#include "Duration.h"
#include "Track.h"
#include "Album.h"
#include "collection.h"
#include "utils.h"

//--------------------------------------INITIALISER
Collection *collection_New(int size)
{
    Collection *c = malloc(sizeof(Collection));
    
    if (c == NULL)
    {
        printf("Not enough memory could be allocated");
    }
    else
    {   //set variables
        c->size = size;
        c->albums = (Album **) malloc(size * sizeof(Album*));//make new album collection
        c->index = 0;

        return c;
    }
}

//-----------------------------------------------FUNCTIONS
Album *collection_getAlbum(Collection *c,int index)
{
    if (index < 0 || index >collection_GetSize(c))
    {
        printf("Index out of bounds!");
    }
    else
    {
        Album *a = c->albums[index];
        return a;
    }
}

void collection_AddAlbum(Collection *c,Album *a)
{
    int size = collection_GetSize(c);
    int ind = collection_GetIndex(c);
    
    //if number of albums == collection size
    if (ind == size)
    {//allocate memory for a new bigger album array
        Album **al = (Album**)malloc((size+10)*sizeof(Album));
        c->size+=10; //make array 10 albums larger
        
        for (int i = 0; i < i; i++)
        {
            al[i] = c->albums[i]; //add each existing album to new array
        }
        al[ind+1] = a; //add new song to new array

        free(c->albums); //free old collection
        c->albums = al; //add new array to collection
        c->index++;
    }
    else //otherwise add new song to collection
    {
        c->albums[ind] = a;
        c->index++;
    }
}

void collection_Sort(Collection *c)
{
    Album **albums = collection_GetAlbums(c);
    int index = collection_GetIndex(c);
    Album *a  = collection_getAlbum(c,0);
   
    qsort(a,index,sizeof(*albums),album_Compare);
}

int collection_GetSize(Collection *c)
{
    return c->size;
}

int collection_GetIndex(Collection *c)
{
    return c->index;
}

 void collection_Printf(FILE *fd,Collection *c)
 {

     int index = collection_GetIndex(c);
     
     for (int i = 0; i < index; i++)
     {
         album_Printf(fd,collection_getAlbum(c,i));
         printf("\n");
     }
 }
 
 Album **collection_GetAlbums(Collection *c)
 {
     return c->albums;
 }
 
 void collection_Free(Collection *c)
 {
     int albums = collection_GetIndex(c);
     
     for (int i = 0; i < albums; i++)
     {
        album_Free(collection_getAlbum(c,i)); //free each album
     }
     
     free(c->albums);
     free(c); //free collection
 }