/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - c.main
 * Last updated - 18/10/2018
 * Function: 
 *          - Facilitates output for Collection, Album, Track, Duration ADT's
 *          - Allows testing for each ADT at high level
 *          - Reads in input from albums.txt
 *          - Creates filled Collection of Albums, Tracks and Durations imported
 *            from header files
 *          - Sorts the collection (based on Album Artist then Title
 *            and writes the sorted Collection to stdout
 */

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "Duration.h"
#include "Track.h"
#include "collection.h"
#include "utils.h"
#include "Album.h"

int main(int argc, char** argv) { 
/*
    
    FILE *file = fopen("Albums.txt","r");


    if (file != NULL)
    {
        
        fseek(file,0,SEEK_END);
        int length = ftell(file);
        fseek(file,0,SEEK_SET);
        char *line = malloc(length);
        char *s1;
        char *s2;
        int currentsize = 0;
        Track **tracks = (Track **) malloc(10 * sizeof(Track)); ;
        Album *currentAlbum;
        
        Collection *c = collection_New(10);

        
        while(fgets(line,length,file) != NULL)
        {
            if (isdigit(line[0]))
            {
                s1 = strtok(line," ");
                s2 = strtok(NULL,"-");
                
                int hour = atoi(strtok(s1,":"));
                int min = atoi(strtok(NULL,":"));
                int sec = atoi(strtok(NULL,":"));
                
                Duration *d = duration_New(hour,min,sec);
                int size = strlen(s2)+1;
                Track *t = track_New(s2,d);
                album_AddTrack(currentAlbum,t);
                currentsize++;
            }
            else
            {
                s1 = strtok(line,":");
                s2 = strtok(NULL,"");
                currentsize = 0;
                Album *a = album_New(s1,s2,currentsize);
                currentAlbum = a;
                collection_AddAlbum(c,a);
            }
        }
    }
*/

    



    
//-------------------------------------------------------------------TESTING
        

    Duration *d = duration_New(0,5,13); //make test duration
    Duration *d2 = duration_New(1,20,0);
    //int ti = duration_AddDuration(d,d2);
    
    char *title = "aaa"; //test title
    char *title2 = "abcd";
    Track *t = track_New(title,d); //make test track
    Track *t2 = track_New(title2,d2);
    
    Duration *d3 = duration_New(0,5,13); //make test duration
    Duration *d4 = duration_New(1,20,0);
    
    Track *t3 = track_New(title,d); //make test track
    Track *t4 = track_New(title2,d2);
    
    Track *t5 = track_New(title2,d4);
    
    char Atitle1[] = "Z";
    char Atitle2[] = "A";
    char Atitle3[] = "C";
    char artist1[] = "Z";
    char artist2[] = "A";
    char artist3[] = "C";

    Album *a  = album_New(artist1,Atitle1,2); //test album
    Album *a2 = album_New(artist2,Atitle2,2); //test album
    Album *a3 = album_New(artist3,Atitle3,2);
    
    album_AddTrack(a,t);
    album_AddTrack(a,t2);
    
    album_AddTrack(a2,t3);
    album_AddTrack(a2,t4);
    
    album_AddTrack(a3,t5);
    
    //comparisons for durations and titles
    int compareTrackD = track_CompareDuration(t,t2);
    int compareTrackT = track_CompareTitle(t,t2);
    
    
    Collection *c  = collection_New(3);
    
    collection_AddAlbum(c,a);
    collection_AddAlbum(c,a2);
    collection_AddAlbum(c,a3);
/*


    //--------------------------Test comparison of track title (and print)
    printf("Total duration in seconds is %d \n",ti);
    
    if(compareTrackD == 1)
    {
        track_Printf(stdout,t);
        printf(" comes after ");
        track_Printf(stdout,t2);
        printf("\n");
    }
    else if(compareTrackD == 0)
    {
        track_Printf(stdout,t);
        printf(" is the same as ");
        track_Printf(stdout,t2);
        printf("\n");
    }
    else
    {
        track_Printf(stdout,t);
        printf(" comes before ");
        track_Printf(stdout,t2);
        printf("\n");
    }
    //--------------------------Test comparison of track durations (and print)
    if(compareTrackT == 1)
    {
        track_Printf(stdout,t);
        printf(" is shorter than ");
        track_Printf(stdout,t2);
        printf("\n");
    }
    else if(compareTrackT == 0)
    {
        track_Printf(stdout,t);
        printf(" is equal to ");
        track_Printf(stdout,t2);
        printf("\n");
    }
    else
    {
        track_Printf(stdout,t);
        printf(" is longer than ");
        track_Printf(stdout,t2);
        printf("\n");
    }

//------------------------------------------------Test album (and print)
    
    album_Printf(stdout,a);

    
//------------------------------------------------------------TEST COLLECTION
   */ 
   collection_Sort(c);
    
   collection_Printf(stdout,c);
    
//------------------------------------------------------------FREE RESOURCES
/*

    duration_Free(d);
    duration_Free(d2);
    track_Free(t);
    track_Free(t2);
    album_Free(a);
    //collection_Free(c);
*/
    




    return (EXIT_SUCCESS);
}

