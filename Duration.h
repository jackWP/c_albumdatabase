/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - duration.h
 * Last updated - 18/10/2018
 * Function: Header file for Duration ADT
 *          - Provide declaration for struct and typedef
 */

#ifndef DURATION_H
#define DURATION_H

#ifdef __cplusplus
extern "C" {
#endif
    
    

    typedef struct DurationStruct 
    {
        int hours; 
        int minutes; 
        int seconds;
    } 
    Duration;

Duration *duration_New(int hour,int min,int sec); //initialiser
void duration_Printf(FILE *fd, Duration *d); //print to stream
void duration_Free(Duration *d); //free memory
int duration_GetSeconds(Duration *d); //return seconds
int duration_GetMinutes(Duration *d); //return minutes
int duration_GetHours(Duration *d); //return hours
int duration_Compare(Duration *d1, Duration *dur2); //1 if >, 0 if =, -1 if <
int duration_TotalSecs(Duration *d); //returns total seconds of duration
int duration_AddDuration(Duration *d1, Duration *d2);//returns total of 
                                                           //two durations

#ifdef __cplusplus
}
#endif

#endif /* DURATION_H */