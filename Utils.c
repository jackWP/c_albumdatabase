/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - utils.c
 * Last updated - 21/10/2018
 * Function: 
 *          - Provides functions for utils.h
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "utils.h"

//-----------------------------------------EFOPEN-------------------------------------
FILE *efopen(char *filename, char *mode) 
{
    FILE *fd = fopen(filename, mode);
    
    if (fd == NULL)
    {
        fprintf(stderr, "%s \n Error number: %d \n",strerror(errno),errno);
        fprintf(stderr, "Error: Unable to open %s in mode %s\n Line %d \n File %s \
                         \n Date %s \n Time %s \n", filename, mode, __LINE__, __FILE__, __DATE__, __TIME__);
        exit(EXIT_FAILURE);
    }
    return fd;
}

