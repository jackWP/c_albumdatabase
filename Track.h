/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - track.h
 * Last updated - 18/10/2018
 * Function: Header file for Track ADT
 *          - Provide declaration for struct and typedef
 */

#ifndef TRACK_H
#define TRACK_H

#include "Duration.h"

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct TrackStruct
    {
        Duration *duration;
        char title[];
    } 
    Track;
    
Track *track_New(char ti[], Duration *dur); //initialiser
void track_Printf(FILE *fd,Track *t); //print to stream
Duration *track_GetDuration(Track *t); //return duration
char *track_GetTitle(Track *t); //return title
void track_Free(Track *t); //free memory
int track_CompareTitle(Track *t1, Track *t2); //1 if >, 0 if =, -1 if <
int track_CompareDuration(Track *t1, Track *t2); //1 if >, 0 if =, -1 if <

#ifdef __cplusplus
}
#endif

#endif /* TRACK_H */