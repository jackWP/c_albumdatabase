/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - album.c
 * Last updated - 08/11/2018
 * Function: ADT for storing an Album with an artist and array of Tracks
 *          - Getters and setters
 *          - Comparisons for artist, title, and number of tracks
 *          - Adding anf removing tracks
 */
#include <stdlib.h> 
#include <stdio.h>  
#include <string.h> 
#include "track.h"
#include "Duration.h"
#include "utils.h"
#include "Album.h"

//-----------------------INITIALISER
Album *album_New(char artist[], char title[], int size)
{
    Album *a = malloc(sizeof(Album)); //allocate memory 
    
    if( a == NULL) //check if memory available
    {
        printf("Not enough memory could be allocated!");
    }
    else
    {   //set variables
        a->artist = artist;
        a->title = title;
        a->index = 0;
        //make new track collection
        a->tracks = (Track **) malloc(size * sizeof(Track)); 

        a->size = size;

        return a;
    }
}
//---------------FUNCTIONS---------------------------------------
void *album_Printf(FILE *fd,Album *a)
{
    fprintf(fd,"%s : %s\n",album_GetArtist(a),album_GetTitle(a));
    
    int songs = album_GetIndex(a);
    
    for (int i = 0; i < songs; i++)
    {
        track_Printf(fd,album_GetTrack(a,i));
        fprintf(fd,"\n");
    }
}
char *album_GetArtist(Album *a)
{
    return a->artist;
}
char *album_GetTitle(Album *a)
{
    return a->title;
}
Track **album_GetTracks(Album *a)
{
    return a->tracks;
}

int album_GetSize(Album *a)
{
    return a->size;
}

void *album_Free(Album *a)
{
    int index = album_GetSize(a);
    
    for (int i = 0; i < index; i ++)
    {
        track_Free(album_GetTrack(a,i));
    }
    
    free(a->tracks);
    free(a->artist);
    free(a->title);
    free(a);
}

void *album_AddTrack(Album *a,Track* t)
{
    int songs = album_GetIndex(a);
    int size = album_GetSize(a);
    
    //if num songs == size, make new array
    if(songs == size)
    { //allocate memory for new array of size + 10
        Track **ts = (Track**) malloc((size+10)*sizeof(Track)); 
        a->size += 10; //increase size by 10
    
        for(int i = 0; i < songs; i++)
        {
            ts[i] = a->tracks[i];
        }
        
        ts[songs+1] = t; //add new song
        a->index ++; //increment num of songs
        free(a->tracks); //free old collection
        a->tracks = ts; //set album track array to equal new array

    }
    else //otherwise add new song and increment num of songs
    {
        a->tracks[songs] = t;
        a->index++;
    }  
}

int album_Compare(const void *a1, const void *a2)
{
    int comp = 0;
    
    Album a = *(Album*)a1;
    Album b = *(Album*)a2;
    
    int cartist = strcmp(album_GetArtist(&a),album_GetArtist(&b));
    //int cartist2 = strcmp(album_GetArtist(b),album_GetArtist(a));
    
    int ctitle = strcmp(album_GetTitle(&a),album_GetTitle(&b));
    //int ctitle2 = strcmp(album_GetTitle(b),album_GetTitle(a));    
    
/*
    if (cartist1 == cartist2)
    {
        
        if(ctitle1 > ctitle2)
        {
            return 1;
        }
        else if (ctitle1 == 0)
        {
            return 0;
        }
        else
        {
            return -1;
        }
        
    }
    else if(cartist1 > cartist2)
    {
        return 1;
    }
    else
    {
        return -1;

    }
 */
    

    if((comp = strcmp(album_GetArtist(&a),album_GetArtist(&b))) != 0)
    {
        return comp;
    }
    if((comp = strcmp(album_GetTitle(&a),album_GetTitle(&b))) != 0)
    {
        return comp;
    }
    return 0;


}

int album_CompareSongNum(Album *a1, Album *a2)
{
    int compare = album_GetIndex(a1) - album_GetIndex(a2);
    
    if(compare > 0) //a1 > a2
    {
        return 1;
    }
    else if(compare == 0) //a1 == a2
    {
        return 0;
    }
    else                //a1 < a2
    {
        return -1; 
    }
}

Track *album_GetTrack(Album *a, int index)
{
    if(index < 0 || index > album_GetSize(a))
    {
        printf("Index out of bounds!");
    }
    else
    {
        Track *t = a->tracks[index];
        return t;        
    }
}
int album_GetIndex(Album *a)
{
    return a->index;
}
