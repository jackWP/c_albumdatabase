/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - utils.h
 * Last updated - 21/10/2018
 * Function: 
 *          - Provides definitions of multiple utility functions and MACROS
 *          - CALLOC - safe calloc
 *          - MALLOC - safe malloc
 *          - EFOPEN - safe file opening with error reporting
 */

#ifndef UTILS_H
#define UTILS_H
#define CALLOC (n, type) ((type* )calloc(n, sizeof(type)))
#define MALLOC (n, type) ((type*) malloc(n,sizeof(type)))
#define SWAP(type,a,b) { type __swap_temp = a; a = b; b = __swap_temp; }

#ifdef __cplusplus
extern "C" {
#endif

char *strerror(int errnum);

FILE *efopen(char *filename, char *mode);


#ifdef __cplusplus
}
#endif

#endif /* UTILS_H */

