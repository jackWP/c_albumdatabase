/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - collection.h
 * Last updated - 18/10/2018
 * Function: Header file for the Collection ADT
 *          - 
 */

#ifndef COLLECTION_H
#define COLLECTION_H

#ifdef __cplusplus
extern "C" {
#endif
#include "Album.h"
    typedef struct CollectionStruct
    {
        Album **albums;
        int size;
        int index;
    }Collection;
    
    //new collection ADT
    Collection *collection_New(int size);
    Album *collection_getAlbum(Collection *c,int index);//return album at index
    void collection_AddAlbum(Collection *c,Album *a); //add album to collection
    void collection_Sort(Collection *c); //sort collection
    int collection_GetSize(Collection *c);// returns collection soze
    void collection_Printf(FILE *fd,Collection *c);//print collection to stream
    void collection_Free(Collection *c); //free collection stuct
    Album **collection_GetAlbums(Collection *c);//return album collection
    int collection_GetIndex(Collection *c);// return current album index

    
#ifdef __cplusplus
}
#endif

#endif /* COLLECTION_H */