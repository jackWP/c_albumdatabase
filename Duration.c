/*
 * Author - Jack Warner-Pankhurst - ajc17dru - 100199337
 * File - duration.c
 * Last updated - 18/10/2018
 * Function: ADT for storing the duration of a song in 4 ints 
 *          (hour,minutes,seconds)
 *          - Initialiser + input error checking
 *          - Getters for each value
 *          - Free struct
 *          - Test harness
 */

#include <stdlib.h> //standard library
#include <stdio.h>  //io functions
#include "duration.h" //duration header file
#include "utils.h"

//--------------INITIALISER--------------------------------------

Duration *duration_New(int hour,int min,int sec)
{
    Duration *d = (Duration*)malloc(sizeof(Duration));
    
    if( d == NULL)
    {
        fprintf(stdout,"Not enough memory could be allocated!");
    }
    
    d->hours = hour;
    d->minutes = min;
    d->seconds = sec;
    
    while (d->seconds >= 60)
    {
        d->seconds -= 60;
        d->minutes++;
    }
    
    while(d->minutes >= 60)
    {
        d->minutes -=60;
        d->hours++;
    }
    return d;
}

//---------------FUNCTIONS---------------------------------------

int duration_GetHours(Duration* d) //return hour of a duration
{
    return d->hours;
}

int duration_GetMinutes(Duration* d) //return minutes of a duration
{
    return d->minutes;
}

int duration_GetSeconds(Duration* d) //return seconds of a duration
{
    return d->seconds;
}

void duration_Free(Duration* d) //free duration struct
{
    free(d);
}

void duration_Printf(FILE *fd, Duration *d) //formatted string output
{
    fprintf(fd,"%02d-%02d-%02d",d->hours,d->minutes,d->seconds);
}
//calculates and returns total seconds of duration ADT
int duration_TotalSecs(Duration* d) 
{
    int total = 0;
    total += (duration_GetHours(d)*60)*60;
    total += duration_GetMinutes(d)*60;
    total += duration_GetSeconds(d);
    return total;
}

//Compares 2 duration ADT's, returns 1 if arg1 is greater than, 0 if equal, and
//-1 if less than arg2
int duration_Compare(Duration* d1, Duration* d2)
{
    int compare = duration_TotalSecs(d1) - duration_TotalSecs(d2);
    
    if(compare > 0) //more than
    {
        return 1;
    }
    else if(compare == 0) //equal
    {
        return 0;
    }
    else            //therefore less than
    {
        return -1;
    }
}
int duration_AddDuration(Duration *d1, Duration *d2)
{
    int total = duration_TotalSecs(d1)+duration_TotalSecs(d2);

    return total;
}